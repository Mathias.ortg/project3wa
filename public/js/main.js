/*  Const & Let*/

const PAINT = 'Pinceaux';
const ERASE = 'Gomme';
const theGridSize = document.forms.gridSize;
const userColor = document.getElementById('colorPicker');
const tileMode = document.getElementById('modeDisplay');
const displayHeight = document.getElementById('gridHeightDisplay');
const displayWidth = document.getElementById('gridWidthDisplay');
const userHeight = document.getElementById('inputHeight');
const userWidth = document.getElementById('inputWidth');
const save = document.getElementById('saveCanvas');
const grid = document.getElementById('pixelCanvas');
const gridCanvas = document.getElementById('gridCanvas');
const hidden = document.getElementById('hiddenBlock')
const url = document.getElementById('pixel_art_link')
const title = document.getElementById('pixel_art_title')
const banner = document.getElementById('pixel_art_banner')
let gridTileMode = PAINT
/*  End const & Let*/

/* Onclick button generate grid */
theGridSize.submitGrid.onclick = function makeGrid(event) {
    event.preventDefault();
    let mouseIsDown = false;
    gridTileMode = PAINT
    rows =  userHeight.value;
    columns = userWidth.value;
    if (rows !== "" && columns !== ""){
        hidden.style.display = "block"
    }
    while (grid.hasChildNodes()) {
        grid.removeChild(grid.lastChild);
    }

    let tableRows = '';
    let r = 1;
    while (r <= rows) {
        tableRows += '<tr>';
        for (let c=1; c <= columns; c++) {
            tableRows += '<td></td>';
        }
        tableRows += '</tr>';
        r += 1;
    }
    grid.insertAdjacentHTML('afterbegin', tableRows);
    document.getElementById('legend').className = "legend";
    grid.addEventListener("click", function(event) {
        event.preventDefault();
        paintEraseTiles(event.target);
    });


    grid.addEventListener('mousedown', function(event) {
        event.preventDefault();
        mouseIsDown = true;
    });

    document.addEventListener('mouseup', function(event) {
        event.preventDefault();
        mouseIsDown = false;
    });

    grid.addEventListener('mouseover', function(event) {
        event.preventDefault();
        if (mouseIsDown) {paintEraseTiles(event.target);}
    });
};

userColor.oninput = function (event){
    gridTileMode = PAINT;
    tileMode.innerHTML = ' ' + gridTileMode;
};
document.getElementById('clearGrid').addEventListener('click', function() {
    let tiles = grid.getElementsByTagName('td');
    for(let i = 0; i <= tiles.length; i++) {
        tiles[i].style.backgroundColor = 'transparent';
    }
});

document.getElementById('mode').addEventListener('click', function(event) {
    gridTileMode = event.target.className.indexOf('paint') >=0 ? PAINT : ERASE;
    tileMode.innerHTML = ' ' + gridTileMode;
});
/* On save button click */
save.addEventListener('click' , function () {

    const tbody = document.getElementById('pixelCanvas').children[0].childNodes
    const array = []
    for ( const tr of tbody) {
        for (const td of tr.childNodes){
            const tdBg = td.style.backgroundColor
            if ( tdBg === "" || tdBg === "transparent" ) {
                array.push({})
            }
            else {
                const rgb = tdBg.split(",")
                const green = rgb[1]
                const red = rgb[0].split("(")[1]
                const blue = rgb[2].split(")")[0]
                array.push({
                    r : red,
                    g : green,
                    b : blue
                })
            }
        }
    }
    let formData = new FormData();
    formData.append("array" , JSON.stringify(array) )
    formData.append("height" , columns )
    formData.append("url" , url.value )
    formData.append("title" , title.value )
    formData.append("banner" , banner.value)
    formData.append("width" , rows )
    fetch(`http://localhost/pixel-art/save` , {
        method : 'POST' ,
        body : formData,
    }).then( (res) => window.location.href = `${res.url}`)
})

function paintEraseTiles(targetCell) {
    if (targetCell.nodeName === 'TD') {
        targetCell.style.backgroundColor = gridTileMode === PAINT ? userColor.value : 'transparent';
    }
}