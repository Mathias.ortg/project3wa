/* CONST & LET*/

const width = document.getElementById("widthArt")
const height = document.getElementById("heightArt")
const id = document.getElementById("idPixel")
const grid = document.getElementById('pixelCanvas');
const draw = JSON.parse(document.getElementById('pixelArt').textContent)
const save = document.getElementById('saveCanvas');
const url = document.getElementById('pixel_art_link')
const title = document.getElementById('pixel_art_title')
const banner = document.getElementById('pixel_art_banner')
const userColor = document.getElementById('colorPicker');
const tileMode = document.getElementById('modeDisplay');
const PAINT = 'Pinceaux';
const ERASE = 'Gomme';
let rows = height.value;
let columns = width.value;
let idPixel = id.value;
/* END of CONST & LET */

/* On DOMContentLoaded  display the pixel art*/
window.addEventListener(('DOMContentLoaded'), () => {
    let mouseIsDown = false;
    rows =  width.textContent;
    columns =height.textContent;
    idPixel = id.textContent;
    while (grid.hasChildNodes()) {
        grid.removeChild(grid.lastChild);
    }
    let tableRows = '';
    let r = 1;
    while (r <= rows) {
        tableRows += '<tr>';
        for (let c = 1; c <= columns; c++) {
            tableRows += '<td></td>';
        }
        tableRows += '</tr>';
        r += 1;
    }
    grid.insertAdjacentHTML('afterbegin', tableRows);
    document.getElementById('legend').className = "legend";
    const tbody = document.getElementById('pixelCanvas').children[0].childNodes
    let i = 0;
    for (const tr of tbody) {
        for (const td of tr.childNodes) {
            td.style.backgroundColor = ` rgb( ${draw[i].r}, ${draw[i].g} , ${draw[i].b})`
            i++;
        }
    }


    document.getElementById('legend').className = "legend";
    grid.classList.toggle('flyItIn'); // fly in effect for grid
    grid.classList.toggle('flyItIn2'); // Twice to trigger reflow
    grid.addEventListener("click", function (event) {
        event.preventDefault();
        paintEraseTiles(event.target);
    });

    grid.addEventListener('mousedown', function (event) {
        event.preventDefault();
        mouseIsDown = event.which === 1 ? true : false;
    });
    document.addEventListener('mouseup', function (event) {
        event.preventDefault();
        mouseIsDown = false;
    });
    grid.addEventListener('mouseover', function (event) {
        // if (mouseIsDown) {paintEraseTiles($(this));}
        event.preventDefault();
        if (mouseIsDown) {
            paintEraseTiles(event.target);
        }
    });
})


document.getElementById('mode').addEventListener('click', function(event) {
    gridTileMode = event.target.className.indexOf('paint') >=0 ? PAINT : ERASE;
    tileMode.innerHTML = ' ' + gridTileMode;
});

save.addEventListener('click' , function () {

    const tbody = document.getElementById('pixelCanvas').children[0].childNodes
    const array = []
    for ( const tr of tbody) {
        for (const td of tr.childNodes){
            const tdBg = td.style.backgroundColor
            if ( tdBg === "" || tdBg === "transparent" ) {
                array.push({})
            }
            else {
                const rgb = tdBg.split(",")
                const green = rgb[1]
                const red = rgb[0].split("(")[1]
                const blue = rgb[2].split(")")[0]
                array.push({
                    r : red,
                    g : green,
                    b : blue
                })
            }
        }
    }
    let formData = new FormData();
    formData.append("array" , JSON.stringify(array) )
    formData.append("id" , idPixel )
    formData.append("url" , url.value )
    formData.append("banner" , banner.value )
    formData.append("title" , title.value )
    fetch(`${window.location.protocol}//${window.location.host}/pixel-art/edit` , {
        method : 'POST' ,
        body : formData,
    }).then( (res) => window.location.href = `${res.url}`)
})

function paintEraseTiles(targetCell) {
    if (targetCell.nodeName === 'TD') {
        targetCell.style.backgroundColor = gridTileMode === PAINT ? userColor.value : 'transparent';
    }
}
