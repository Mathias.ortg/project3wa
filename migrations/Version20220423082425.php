<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220423082425 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pixel_art DROP FOREIGN KEY FK_9268E6B0684EC833');
        $this->addSql('ALTER TABLE pixel_art ADD CONSTRAINT FK_9268E6B0684EC833 FOREIGN KEY (banner_id) REFERENCES banner (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pixel_art DROP FOREIGN KEY FK_9268E6B0684EC833');
        $this->addSql('ALTER TABLE pixel_art ADD CONSTRAINT FK_9268E6B0684EC833 FOREIGN KEY (banner_id) REFERENCES banner (id) ON DELETE CASCADE');
    }
}
