FROM php:8.0.2-fpm

# PHP extensions
RUN buildDeps=" \
        libicu-dev \
        zlib1g-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libxml2-dev \
        librabbitmq-dev \
        libssh-dev \
        zlib1g \
        libpng-dev \
    " \
    && apt-get update -qq && apt-get install -y --force-yes -q --no-install-recommends \
        $buildDeps \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install \
        intl \
        pdo_mysql \
        bcmath \
        sockets \
        soap \
        opcache \
        gd \
    && pecl install amqp \
    && docker-php-ext-enable amqp

COPY docker/php/php.ini /usr/local/etc/php/php.ini
COPY docker/php/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions imagick

# System
RUN apt-get update -qq && apt-get install -y --force-yes -q --no-install-recommends \
       htop git unzip wget ssh gnupg \
    && rm -rf /var/lib/apt/lists/*

# Composer
# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER 1
COPY docker/install/install-composer.sh /usr/local/bin/docker-app-install-composer
RUN chmod +x /usr/local/bin/docker-app-install-composer
RUN docker-app-install-composer && mv composer.phar /usr/local/bin/composer
RUN composer self-update 1.7.2

# Deployer
RUN curl -LO https://deployer.org/deployer.phar
RUN mv deployer.phar /usr/local/bin/dep
RUN chmod +x /usr/local/bin/dep

# Sendmail
RUN apt-get update && apt-get install -y sendmail
ADD docker/install/hosts.sh /root/hosts.sh

# Add the application
WORKDIR /app

RUN mkdir /root/.ssh
COPY docker/ssh/config /root/.ssh/config

COPY docker/docker-entrypoint.sh /usr/local/bin/docker-app-entrypoint
RUN chmod +x /usr/local/bin/docker-app-entrypoint

ENTRYPOINT ["docker-app-entrypoint"]
CMD ["php-fpm"]
