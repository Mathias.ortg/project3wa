<?php

namespace App\Services;

use ImagickPixel;

class PixelArtService {

    # Generater a pixel art
    public function generatePixelArt( $height ,$width , $pixelArray) {
        $pixelIMG = new \Imagick();
        $pixelIMG->newImage($height ,$width , new ImagickPixel("#ffffff"));
        $pixelIMG->setImageFormat("png");
        $array = json_decode($pixelArray);
        $pixelIterator = $pixelIMG->getPixelIterator();
        $i = 0;
        foreach ($pixelIterator as $row => $pixels) { /* Loop through pixel rows */
            foreach ($pixels as $column => $pixel) { /* Loop through the pixels in the row (columns) */
                /** @var $pixel ImagickPixel */
                if (isset($array[$i]->r)){
                    $pixel->setColor("rgba(".$array[$i]->r.", ".$array[$i]->g.", ".$array[$i]->b.",0)"); /* Paint every second pixel black*/
                }
                $i++;
            }
            $pixelIterator->syncIterator(); /* Sync the iterator, this is important to do on each iteration */
        }
        return $pixelIMG;
    }
}