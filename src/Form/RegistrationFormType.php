<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email' , RepeatedType::class , [
                'type'=> TextType::class,
                'first_options'  => [
                    'label' => 'Email',
                    'attr' => [
                        'autocomplete' => 'new-password',
                        'form-control',
                        'placeholder'=> "Adresse email"
                    ],
                ],
                'second_options' => [
                    'label' => 'Email',
                    'attr' => [
                        'autocomplete' => 'new-password',
                        'form-control',
                        'placeholder'=> "Répetez l'adresse email"
                    ],
                ],
                'invalid_message' => 'Veuillez saisir des emails identiques',
            ])
            ->add('plainPassword', RepeatedType::class ,[
                'type' => PasswordType::class,
                'required'=> true,
                'first_options'  => [
                    'label' => 'Mot de passe',
                    'attr' => [
                        'autocomplete' => 'new-password',
                        'form-control',
                        'placeholder'=> "Mot de passe"
                    ],
                ],
                'second_options' => [
                    'label' => 'Mot de passe',
                    'attr' => [
                        'autocomplete' => 'new-password',
                        'form-control',
                        'placeholder'=> "Répetez le mot de passe"
                    ],
                ],
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit faire au moins 6 caracteres',
                        'max' => 4096,
                    ]),
                ],
                'invalid_message' => 'Veuillez saisir des mots de passes identiques',
            ])
            ->add('firstName' , TextType::class , [
                'label'=>'Prénom' ,
                'attr' =>  [
                    'placeholder'=>"Prénom",
                    'form-control'
                ]
            ])
            ->add('lastName' , TextType::class , [
                'label'=>'Nom' ,
                'attr' =>  [
                    'placeholder'=>"Nom",
                    'form-control'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
