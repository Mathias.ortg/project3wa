<?php

namespace App\Form;

use App\Entity\Banner;
use App\Entity\PixelArt;
use App\Repository\BannerRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PixelArtType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('link', TextType::class , [
                'required'=> true,
                'attr'=> [
                    'placeholder' => "URL entière du site",
                    'class' =>'form-control'
                ]
            ])
            ->add('title' , TextType::class , [
                'required'=> true,
                'attr'=> [
                    'placeholder' => "Déscription de votre site",
                    'class' =>'form-control'
                ]
            ])
            ->add('banner' , EntityType::class , [
                'class' => Banner::class,
                'query_builder' => function (BannerRepository $repo) {
                return $repo->createQueryBuilder('b');
                },
                'choice_label' => function ($b) {
                    return $b->getName();
                },
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PixelArt::class,
        ]);
    }
}
