<?php

namespace App\Form;

use App\Entity\Banner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BannerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'form-control',
                    'placeholder' => "Nom de la bannière"
                ],
            ])
            ->add('site', TextType::class, [
                'attr' => [
                    'form-control',
                    'placeholder' => "URL entière du site"
                ],
            ])
            ->add('width', IntegerType::class, [
                'attr' => [
                    'form-control',
                    'placeholder' => "Largeur de la bannière"
                ],
            ])
            ->add('height', IntegerType::class, [
                'attr' => [
                    'form-control',
                    'placeholder' => "Hauteur de la bannière"
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Banner::class,
        ]);
    }
}
