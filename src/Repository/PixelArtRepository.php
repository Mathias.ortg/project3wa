<?php

namespace App\Repository;

use App\Entity\PixelArt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PixelArt|null find($id, $lockMode = null, $lockVersion = null)
 * @method PixelArt|null findOneBy(array $criteria, array $orderBy = null)
 * @method PixelArt[]    findAll()
 * @method PixelArt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PixelArtRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PixelArt::class);
    }

    // /**
    //  * @return PixelArt[] Returns an array of PixelArt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PixelArt
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
