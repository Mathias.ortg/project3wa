<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:create-admin',
    description: 'Création du compte admin',
)]
class CreateAdminCommand extends Command
{
    private UserRepository $userRepository;
    private UserPasswordHasherInterface $userPasswordHasher;

    /**
     * @param UserRepository $repository
     */
    public function __construct (UserRepository $userRepository , UserPasswordHasherInterface $userPasswordHasher)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->userPasswordHasher = $userPasswordHasher;
    }
    /* generate a Admin User*/
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $adminExist = $this->userRepository->findOneBy(['email' => "admin"]);
        $io = new SymfonyStyle($input, $output);

        if ($adminExist == null) {
            //Generate random password
            $pass = substr(md5(random_bytes(10)),0,12);
            //Create the new User
            $admin = new User();
            $admin
                ->setEmail('admin')
                ->setPassword($this->userPasswordHasher->hashPassword(
                    $admin,
                    $pass
                ))
                ->setRoles(["ROLE_ADMIN"])
                ->setFirstName("admin")
                ->setLastName("admin")
            ;
            $this->userRepository->save($admin);
            $io->success('Admin crée avec le mot de passe ' .$pass);
        }else {
            $pass = substr(md5(random_bytes(10)),0,12);
            $adminExist ->setPassword($this->userPasswordHasher->hashPassword(
                $adminExist,
                $pass
            ));
            $this->userRepository->save($adminExist);
            $io->success('Mot de passe admin modifié , Mot de pass ' .$pass);

        }
        return Command::SUCCESS;
    }
}
