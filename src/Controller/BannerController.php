<?php

namespace App\Controller;

use App\Entity\Banner;
use App\Repository\BannerRepository;
use App\Repository\UserRepository;
use App\Services\PixelArtService;
use Imagick;
use ImagickPixel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/banner')]
class BannerController extends AbstractController
{
    # Display all the banner
    #[Route('/', name: 'banner_index', methods: ['GET' , 'POST'])]
    public function index(BannerRepository $bannerRepository, UserRepository $userRepository )
    {
        return $this->render('banner/index.html.twig', [
            'banners' => $bannerRepository->findAll(),
        ]);
    }
    # Generate the banner with dthe pixel art
    #[Route('/banner-kit-{banner}.js', name: 'banner_kit')]
    public function bannerKit(Banner $banner) {

        return $this->render('banner/banner-kit.js.twig' , [
            'banner' => $banner
        ]);
    }
    # show one banner
    #[Route('/{banner}', name: 'banner_show', methods: ['GET'])]
    public function show(Banner $banner): Response
    {
        return $this->render('banner/show.html.twig', [
            'banner' => $banner,
        ]);
    }
}
