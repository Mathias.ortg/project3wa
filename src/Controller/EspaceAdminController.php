<?php

namespace App\Controller;

use App\Entity\Banner;
use App\Form\BannerType;
use App\Repository\PixelArtRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/espace-admin')]
# Access of this page secure by the security.yml
class EspaceAdminController extends AbstractController
{
    # Create a new banner
    #[Route('/new', name: 'banner_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $banner = new Banner();
        $form = $this->createForm(BannerType::class, $banner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($banner);
            $entityManager->flush();

            return $this->redirectToRoute('banner_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('banner/new.html.twig', [
            'banner' => $banner,
            'form' => $form,
        ]);
    }

    # Edit a banner
    #[Route('/edit/{banner}', name: 'edit_banner')]
        public function edit(Request $request, Banner $banner, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(BannerType::class, $banner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('banner_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('banner/edit.html.twig', [
            'banner' => $banner,
            'form' => $form,
        ]);
    }
    # delete a banner
    #[Route('/delete/{banner}', name: 'banner_delete', methods: ['POST'])]
    public function delete(Request $request, Banner $banner, PixelArtRepository $pixelArt, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$banner->getId(), $request->request->get('_token'))) {
            $entityManager->remove($banner);
            $entityManager->flush();
        }

        return $this->redirectToRoute('banner_index', [], Response::HTTP_SEE_OTHER);
    }

}
