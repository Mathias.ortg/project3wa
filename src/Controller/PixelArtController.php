<?php

namespace App\Controller;

use App\Entity\Banner;
use App\Entity\PixelArt;
use App\Entity\User;
use App\Form\PixelArtType;
use App\Repository\BannerRepository;
use App\Repository\PixelArtRepository;
use App\Services\PixelArtService;
use Doctrine\ORM\EntityManagerInterface;
use ImagickPixel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Tests\Fixtures\ToString;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


#[Route('/pixel-art')]
class   PixelArtController extends AbstractController
{
    # New pixel art
    #[Route('/new', name: 'pixel_art-new',  methods: ['GET', 'POST'])]
    public function index()
    {

        $form = $this->createForm(PixelArtType::class );
        return $this->render('pixel_art/new.html.twig' , [
            'form'=>$form->createView()
        ]);
    }
    # Save the data of the pixel art
    #[Route('/save' , name: 'pixel_pub_save' ,  methods: ['GET', 'POST'] )]
    public function savePixel( Request $request ,EntityManagerInterface $entityManager , BannerRepository $bannerRepository):Response {
        /** @var User $user */
        $user = $this->getUser();
        $pixelSave = new PixelArt();
        $pixelSave->setDraw( $request->request->get("array"))
                    ->setBanner( $bannerRepository->find($request->request->get("banner")))
                    ->setWidth($request->request->get("width"))
                    ->setLink($request->request->get("url"))
                    ->setTitle($request->request->get("title"))
                    ->setCustomer($user)
                    ->setHeight($request->request->get("height"))
            ;
        $entityManager->persist($pixelSave);
        $entityManager->flush();

        return  $this->redirectToRoute('pixel_art_place' , ['pixelArt' => $pixelSave->getId()]);
    }
    # Edit a pixel art
    #[Route('/edit' , name: 'pixel_pub_edit' ,  methods: ['GET', 'POST'] )]
    public function editPixelJs(Request $request , EntityManagerInterface $entityManager ,
                                PixelArtRepository $pixelRepository ,BannerRepository $bannerRepository):Response {
        $pixel = $pixelRepository->find($request->request->get("id"));
        $pixel->setDraw( $request->request->get("array"));
        $pixel->setTitle($request->request->get("title"));
        $pixel->setLink($request->request->get("url"));
        $pixel->setBanner($bannerRepository->find($request->request->get("banner")));
        $entityManager->persist($pixel);
        $entityManager->flush();

        return  $this->redirectToRoute("pixel_art_place" , ['pixelArt' => $pixel->getId()]);
    }
    # Display all the pixel art by User
    #[Route('/', name: 'pixel-art',  methods: ['GET', 'POST'])]
    public function pixelArt(PixelArtRepository $repository )
    {
        /** @var User $user */
        $user = $this->getUser();
       $pixels = $repository->findBy(['customer' => $user]);
        return $this->render('pixel_art/pixel_art.html.twig', [
            'pixels'=> $pixels,
        ]);
    }
    # Edit the pixel art
    #[Route('/edit/{pixelArt}', name: 'pixel_art_edit', methods: ['GET', 'POST'])]
    public function editPixelArt(PixelArt $pixelArt , PixelArtRepository $pubRepository)
    {
        /** @var User $user */
        $user = $this->getUser()->getId();
        if ($pixelArt->getCustomer()->getId() == $user)
        {
            $form = $this->createForm(PixelArtType::class, $pixelArt );

            return $this->render('pixel_art/edit.html.twig', [
                'pixel' => $pixelArt,
                'form'=> $form->createView()
            ]);
        } else {
            $this->addFlash('danger' , "Vous n'avez pas les droits pour realiser cette action");
            return $this->redirectToRoute('pixel-art');
        }

    }
    #Deelte the pixel art
    #[Route('/delete/{id}', name: 'pixel_art_delete')]
    public function delete(Request $request, PixelArt $pixelArt, EntityManagerInterface $entityManager): Response
    {
        /** @var User $user */
        $user = $this->getUser()->getId();
        if ($pixelArt->getCustomer()->getId() == $user) {
            $entityManager->remove($pixelArt);
            $entityManager->flush();

            return $this->redirectToRoute('pixel-art');
        } else {
                $this->addFlash('danger' , "Vous n'avez pas les droits pour realiser cette action");
                return $this->redirectToRoute('pixel-art');
            }

    }
    #Display the pixel art
    #[Route('/{pixelPub}.png', name: 'pixel_art_image', methods: ['GET', 'POST'])]
    public function createPixelArt(PixelArt $pixelPub , PixelArtService $pixelArtService): Response
    {
        $pixelIMG = $pixelArtService->generatePixelArt($pixelPub->getHeight(),$pixelPub->getWidth(),$pixelPub->getDraw());
        $response = new Response();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, "toto.png");
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'image/png');
        $response->setContent($pixelIMG->getImageBlob());
        return $response;
    }
    # Set up the pixel art on the banner
    #[Route('/placer/{pixelArt}', name: 'pixel_art_place')]
    public function placerPixelArt(PixelArt $pixelArt , EntityManagerInterface $entityManager)
    {

        $pixelArt->setCoords([]);
        $entityManager->persist($pixelArt);
        $entityManager->flush();
        return $this->render('pixel_art/placer.html.twig' ,[
            'pixelArt' => $pixelArt
        ]);
    }
    #Save the set up of the pixel art on the banner
    #[Route('/placer/{pixelArt}/save', name: 'pixel_art_place_save')]
    public function placePixelArtSave( PixelArt $pixelArt ,Request $request , EntityManagerInterface $entityManager) {

        if ( $request->get('coordX') == "") {
            $this->addFlash('warning' , "Vous devez obligatoirement definir un emplacement pour votre pixel art");
            return $this->redirectToRoute('pixel_art_place' , ['pixelArt' => $pixelArt->getId()]);
        }
        $pixelArt->setCoords([$request->get("coordX") , $request->get("coordY")]);
        $entityManager->persist($pixelArt);
        $entityManager->flush();

        return $this->redirectToRoute('pixel-art');
    }

}
