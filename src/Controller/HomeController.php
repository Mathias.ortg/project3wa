<?php

namespace App\Controller;

use App\Entity\Banner;
use App\Entity\PixelArt;
use App\Repository\BannerRepository;
use App\Repository\PixelArtRepository;
use App\Services\PixelArtService;
use Imagick;
use ImagickPixel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    # Display one banner
    #[Route('/', name: 'home')]
    public function index(BannerRepository $bannerRepository): Response
    {
        return $this->render('base.html.twig', [
            'banners' =>$bannerRepository->find(1)
        ]);
    }
    # Tutorial page
    #[Route('/tutoriel', name: 'tutoriel')]
    public function tutoriel()
    {
        return $this->render('home/tutoriel.html.twig');
    }
    # Display a banner with the pixel art at the coords
    #[Route('/avatar/{banner}.png', name: 'banner-img', methods: ['GET'])]
    public function generateImage(Banner $banner , PixelArtService $pixelArtService)
    {

        $bannerImg = new \Imagick();
        $bannerImg->newImage($banner->getWidth(), $banner->getHeight(), new ImagickPixel("#A9A9A9"));
        $bannerImg->setImageFormat("png");
        //TODO commentaire
        foreach ($banner->getPixelPubs() as $pixelPub) {
            if ($pixelPub->getCoords() != null) {
                $coords = $pixelPub->getCoords();
                $bannerImg->compositeImage(
                    $pixelArtService->generatePixelArt($pixelPub->getHeight(), $pixelPub->getWidth(), $pixelPub->getDraw()),
                    imagick::COMPOSITE_OVER,
                    $coords[0],
                    $coords[1]
                );
            }
        }
        $response = new Response();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, "toto.png");
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'image/png');
        $response->setContent($bannerImg->getImageBlob());
        return $response;
    }

}
