<?php

namespace App\Entity;

use App\Repository\BannerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BannerRepository::class)]
class Banner
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $site;

    #[ORM\Column(type: 'integer')]
    private $width;

    #[ORM\Column(type: 'integer')]
    private $height;

    #[ORM\OneToMany(mappedBy: 'banner', targetEntity: Order::class)]
    private $orders;

    #[ORM\OneToMany(mappedBy: 'banner', targetEntity: PixelArt::class )]
    private $pixelPubs;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->pixelPubs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(string $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setBanner($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getBanner() === $this) {
                $order->setBanner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PixelArt>
     */
    public function getPixelPubs(): Collection
    {
        return $this->pixelPubs;
    }

    public function addPixelPub(PixelArt $pixelPub): self
    {
        if (!$this->pixelPubs->contains($pixelPub)) {
            $this->pixelPubs[] = $pixelPub;
            $pixelPub->setBanner($this);
        }

        return $this;
    }

    public function removePixelPub(PixelArt $pixelPub): self
    {
        if ($this->pixelPubs->removeElement($pixelPub)) {
            // set the owning side to null (unless already changed)
            if ($pixelPub->getBanner() === $this) {
                $pixelPub->setBanner(null);
            }
        }

        return $this;
    }
}
