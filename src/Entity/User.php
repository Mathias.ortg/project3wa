<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'Il existe deja un compte avec ces identifiants')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $email;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\Column(type: 'string', length: 255)]
    private $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    private $lastName;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: PixelArt::class)]
    private $pixelPubs;

    public function __construct()
    {
        $this->pixelPubs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(){}

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Collection<int, PixelArt>
     */
    public function getPixelPubs(): Collection
    {
        return $this->pixelPubs;
    }

    public function addPixelPub(PixelArt $pixelPub): self
    {
        if (!$this->pixelPubs->contains($pixelPub)) {
            $this->pixelPubs[] = $pixelPub;
            $pixelPub->setCustomer($this);
        }

        return $this;
    }

    public function removePixelPub(PixelArt $pixelPub): self
    {
        if ($this->pixelPubs->removeElement($pixelPub)) {
            // set the owning side to null (unless already changed)
            if ($pixelPub->getCustomer() === $this) {
                $pixelPub->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return String
     */
    public function getRoleUser(){

        if (in_array("ROLE_ADMIN", $this->getRoles())) {
            return  "ROLE_ADMIN";
        } else if (in_array("ROLE_USER", $this->getRoles())) {
            return  "ROLE_USER";
       } else {
            return "";
        }
    }

}
