<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'orders')]
    private $customer;

    #[ORM\ManyToOne(targetEntity: Banner::class, inversedBy: 'orders')]
    private $banner;

    #[ORM\OneToMany(mappedBy: 'order', targetEntity: PixelArt::class)]
    private $pixelPubs;

    public function __construct()
    {
        $this->pixelPubs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getBanner(): ?Banner
    {
        return $this->banner;
    }

    public function setBanner(?Banner $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * @return Collection<int, PixelArt>
     */
    public function getPixelPubs(): Collection
    {
        return $this->pixelPubs;
    }

    public function addPixelPub(PixelArt $pixelPub): self
    {
        if (!$this->pixelPubs->contains($pixelPub)) {
            $this->pixelPubs[] = $pixelPub;
            $pixelPub->setOrder($this);
        }

        return $this;
    }

    public function removePixelPub(PixelArt $pixelPub): self
    {
        if ($this->pixelPubs->removeElement($pixelPub)) {
            // set the owning side to null (unless already changed)
            if ($pixelPub->getOrder() === $this) {
                $pixelPub->setOrder(null);
            }
        }

        return $this;
    }
}
