<?php

namespace App\Entity;

use App\Repository\PixelArtRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PixelArtRepository::class)]
class PixelArt
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Banner::class, inversedBy: 'pixelPubs' )]
    #[ORM\JoinColumn(onDelete : "SET NULL")]
    private $banner;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'pixelPubs')]
    private $customer;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'pixelPubs')]
    private $order;

    #[ORM\Column(type: 'array' ,  nullable: true)]
    private $coords = [];

    #[ORM\Column(type: 'string', length: 255 , nullable: true)]
    private $link;

    #[ORM\Column(type: 'string', length: 255 , nullable: true )]
    private $title;

    #[ORM\Column(type: 'text', nullable: true)]
    private $draw;

    #[ORM\Column(type: 'string', length: 255)]
    private $width;

    #[ORM\Column(type: 'string', length: 255)]
    private $height;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBanner(): ?Banner
    {
        return $this->banner;
    }

    public function setBanner(?Banner $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    public function getCustomer(): ?User
    {
        return $this->customer;
    }

    public function setCustomer(?User $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getCoords(): ?array
    {
        return $this->coords;
    }

    public function setCoords(array $coords): self
    {
        $this->coords = $coords;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param mixed $draw
     * @return PixelArt
     */
    public function setDraw($draw)
    {
        $this->draw = $draw;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDraw()
    {
        return $this->draw;
    }

    /**
     * @param mixed $width
     * @return PixelArt
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $height
     * @return PixelArt
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

}
